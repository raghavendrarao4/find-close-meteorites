# find-close-meteorites

Demo project that uses python & NASA data to find meteor landing sites

## Running with python3

This project requires Python 3 and the requests package.

`python3 find_meteors.py`

## Running with pipenv

First install pipenv. Then:

```
   pipenv install
   pipenv run python3 meteors/find_meteors.py
```
